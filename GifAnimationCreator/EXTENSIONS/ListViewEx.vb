﻿Imports System.ComponentModel
Imports GifAnimationCreator.Win32

'''''Dragging items in a ListView control with visual insertion guides
'''''http://www.cyotek.com/blog/dragging-items-in-a-listview-control-with-visual-insertion-guides

Namespace Extensions

    Public Class ListViewEx
        Inherits ListView

        Enum InsertionModes
            Before
            After
        End Enum

        Private _insertionLineColor As Color

        Private elv As Boolean = False

        Public Sub New()
            Me.DoubleBuffered = True
            Me.InsertionLineColor = Color.DeepSkyBlue
            Me.InsertionIndex = -1
        End Sub

        Protected Overrides Sub OnDragDrop(drgevent As DragEventArgs)
            If Me.IsRowDragInProgress Then
                Try
                    Dim dropItem As ListViewItem
                    dropItem = If(Me.InsertionIndex <> -1, Me.Items(Me.InsertionIndex), Nothing)

                    If dropItem IsNot Nothing Then
                        Dim dragItem As ListViewItem
                        Dim dropIndex As Integer
                        dragItem = CType(drgevent.Data.GetData(GetType(ListViewItem)), ListViewItem)
                        dropIndex = dropItem.Index

                        If dragItem.Index < dropIndex Then
                            dropIndex -= 1
                        End If

                        If Me.InsertionMode = InsertionModes.After AndAlso dragItem.Index < Me.Items.Count - 1 Then
                            dropIndex += 1
                        End If

                        If dropIndex <> dragItem.Index Then
                            Dim clientPoint As Point
                            clientPoint = Me.PointToClient(New Point(drgevent.X, drgevent.Y))
                            Me.Items.Remove(dragItem)
                            Me.Items.Insert(dropIndex, dragItem)
                            Me.SelectedItem = dragItem
                        End If
                    End If

                Finally
                    Me.InsertionIndex = -1
                    Me.IsRowDragInProgress = False
                    Me.Invalidate()
                End Try
            End If

            MyBase.OnDragDrop(drgevent)
        End Sub

        Protected Overrides Sub OnDragLeave(ByVal e As EventArgs)
            Me.InsertionIndex = -1
            Me.Invalidate()
            MyBase.OnDragLeave(e)
        End Sub

        Protected Overrides Sub OnDragOver(ByVal drgevent As DragEventArgs)
            If Me.IsRowDragInProgress Then
                Dim insertionI As Integer
                Dim insertionM As InsertionModes
                Dim dropItem As ListViewItem
                Dim clientPoint As Point
                clientPoint = Me.PointToClient(New Point(drgevent.X, drgevent.Y))
                dropItem = Me.GetItemAt(0, Math.Min(clientPoint.Y, Me.Items(Me.Items.Count - 1).GetBounds(ItemBoundsPortion.Entire).Bottom - 1))

                If dropItem IsNot Nothing Then
                    Dim bounds As Rectangle
                    bounds = dropItem.GetBounds(ItemBoundsPortion.Entire)
                    insertionI = dropItem.Index
                    insertionM = If(clientPoint.Y < bounds.Top + (bounds.Height / 2), InsertionModes.Before, InsertionModes.After)
                    drgevent.Effect = DragDropEffects.Move
                Else
                    insertionI = -1
                    insertionM = Me.InsertionMode
                    drgevent.Effect = DragDropEffects.None
                End If

                If insertionI <> Me.InsertionIndex OrElse insertionM <> Me.InsertionMode Then
                    Me.InsertionMode = insertionM
                    Me.InsertionIndex = insertionI
                    Me.Invalidate()
                End If
            End If

            MyBase.OnDragOver(drgevent)
        End Sub

        Protected Overrides Sub OnItemDrag(ByVal e As ItemDragEventArgs)
            If Me.Items.Count > 1 Then
                Me.IsRowDragInProgress = True
                Me.DoDragDrop(e.Item, DragDropEffects.Move)
            End If

            MyBase.OnItemDrag(e)
        End Sub

        Protected Overrides Sub OnPaint(ByVal e As PaintEventArgs)
            MyBase.OnPaint(e)
        End Sub

        <DebuggerStepThrough>
        Protected Overrides Sub WndProc(ByRef m As Message)
            MyBase.WndProc(m)

            Select Case m.Msg
                Case NativeConst.WM_PAINT
                    Me.OnWmPaint(m)
            End Select
        End Sub

        <Category("Appearance")>
        <DefaultValue(GetType(Color), "Red")>
        Public Overridable Property InsertionLineColor As Color
            Get
                Return _insertionLineColor
            End Get
            Set(ByVal value As Color)

                If Me.InsertionLineColor <> value Then
                    _insertionLineColor = value
                End If
            End Set
        End Property

        <Browsable(False)>
        <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
        Public Property SelectedItem As ListViewItem
            Get
                Return If(Me.SelectedItems.Count <> 0, Me.SelectedItems(0), Nothing)
            End Get
            Set(ByVal value As ListViewItem)
                Me.SelectedItems.Clear()

                If value IsNot Nothing Then
                    value.Selected = True
                End If

                Me.FocusedItem = value
            End Set
        End Property

        Protected Property InsertionIndex As Integer
        Protected Property InsertionMode As InsertionModes
        Protected Property IsRowDragInProgress As Boolean

        Protected Overridable Sub OnWmPaint(ByRef m As Message)
            If Not elv Then
                NativeMethods.SetWindowTheme(Handle, "explorer", Nothing)
                NativeMethods.SendMessage(Handle, NativeConst.LVM_SETEXTENDEDLISTVIEWSTYLE, NativeConst.LVS_EX_DOUBLEBUFFER, NativeConst.LVS_EX_DOUBLEBUFFER)
                elv = True
            End If
            Me.DrawInsertionLine()
        End Sub

        Private Sub DrawInsertionLine()
            If Me.InsertionIndex <> -1 Then
                Dim index As Integer
                index = Me.InsertionIndex

                If index >= 0 AndAlso index < Me.Items.Count Then
                    Dim bounds As Rectangle
                    Dim x As Integer
                    Dim y As Integer
                    Dim width As Integer
                    bounds = Me.Items(index).GetBounds(ItemBoundsPortion.Entire)
                    x = 0
                    y = If(Me.InsertionMode = InsertionModes.Before, bounds.Top, bounds.Bottom)
                    width = Math.Min(bounds.Width - bounds.Left, Me.ClientSize.Width)
                    Me.DrawInsertionLine(x, y, width)
                End If
            End If
        End Sub

        Private Sub DrawInsertionLine(ByVal x1 As Integer, ByVal y As Integer, ByVal width As Integer)
            Using g As Graphics = Me.CreateGraphics()
                Dim leftArrowHead As Point()
                Dim rightArrowHead As Point()
                Dim arrowHeadSize As Integer
                Dim x2 As Integer
                x2 = x1 + width
                arrowHeadSize = 7
                leftArrowHead = {New Point(x1, y - (arrowHeadSize / 2)), New Point(x1 + arrowHeadSize, y), New Point(x1, y + (arrowHeadSize / 2))}
                rightArrowHead = {New Point(x2, y - (arrowHeadSize / 2)), New Point(x2 - arrowHeadSize, y), New Point(x2, y + (arrowHeadSize / 2))}

                Using pen As Pen = New Pen(Me.InsertionLineColor)
                    g.DrawLine(pen, x1, y, x2 - 1, y)
                End Using

                Using brush As Brush = New SolidBrush(Me.InsertionLineColor)
                    g.FillPolygon(brush, leftArrowHead)
                    g.FillPolygon(brush, rightArrowHead)
                End Using
            End Using
        End Sub
    End Class


End Namespace

