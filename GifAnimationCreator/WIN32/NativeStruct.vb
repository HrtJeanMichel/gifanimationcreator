﻿Imports System.Runtime.InteropServices

Namespace Win32
    Public NotInheritable Class NativeStruct

#Region " HELPERS.ICONS "
        <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Auto)>
        Public Structure SHFILEINFO
            Public hIcon As IntPtr
            Public iIcon As Integer
            Public dwAttributes As UInteger
            <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=260)>
            Public szDisplayName As String
            <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=80)>
            Public szTypeName As String
        End Structure
#End Region

    End Class
End Namespace
