﻿Namespace Win32
    Public NotInheritable Class NativeEnum

#Region " HELPERS.ICONS "
        <Flags>
        Public Enum SHGFI
            SHGFI_ICON = &H100
            SHGFI_DISPLAYNAME = &H200
            SHGFI_TYPENAME = &H400
            SHGFI_ATTRIBUTES = &H800
            SHGFI_ICONLOCATION = &H1000
            SHGFI_EXETYPE = &H2000
            SHGFI_SYSICONINDEX = &H4000
            SHGFI_LINKOVERLAY = &H8000
            SHGFI_SELECTED = &H10000
            SHGFI_ATTR_SPECIFIED = &H20000
            SHGFI_LARGEICON = &H0
            SHGFI_SMALLICON = &H1
            SHGFI_OPENICON = &H2
            SHGFI_SHELLICONSIZE = &H4
            SHGFI_PIDL = &H8
            SHGFI_USEFILEATTRIBUTES = &H10
            SHGFI_ADDOVERLAYS = &H20
            SHGFI_OVERLAYINDEX = &H40
            FILE_ATTRIBUTE_NORMAL = &H80
        End Enum
#End Region

    End Class
End Namespace
