﻿
Namespace Win32
    Public NotInheritable Class NativeConst

#Region " EXTENSIONS.LISTVIEWEX "
        Public Const WM_PAINT As Integer = &HF
        Public Const LVM_FIRST As Integer = 4096
        Public Const LVM_SETEXTENDEDLISTVIEWSTYLE As Integer = LVM_FIRST + 54
        Public Const LVS_EX_DOUBLEBUFFER As Integer = 65536
#End Region

    End Class

End Namespace
