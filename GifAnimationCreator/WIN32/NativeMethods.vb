﻿Imports System.Runtime.InteropServices

Namespace Win32
    Public NotInheritable Class NativeMethods

#Region " EXTENSIONS.LISTVIEWEX "
        <DllImport("user32.dll", CharSet:=CharSet.Auto)>
        Public Shared Function SendMessage(hWnd As IntPtr, msg As Integer, wParam As Integer, lParam As Integer) As IntPtr
        End Function

        <DllImport("uxtheme.dll", CharSet:=CharSet.Unicode)>
        Public Shared Function SetWindowTheme(hWnd As IntPtr, pszSubAppName As String, pszSubIdList As String) As Integer
        End Function
#End Region

#Region " HELPERS.ICONS "
        <DllImport("Shell32", CharSet:=CharSet.Auto)>
        Public Shared Function ExtractIconEx(<MarshalAs(UnmanagedType.LPTStr)> lpszFile As String, nIconIndex As Integer, phIconLarge As IntPtr(), phIconSmall As IntPtr(), nIcons As Integer) As Integer
        End Function

        <DllImport("Shell32", CharSet:=CharSet.Auto)>
        Public Shared Function SHGetFileInfo(pszPath As String, dwFileAttributes As Integer, psfi As NativeStruct.SHFILEINFO, cbFileInfo As Integer, uFlags As NativeEnum.SHGFI) As IntPtr
        End Function
#End Region

    End Class
End Namespace
