﻿Imports Gif.Components
Imports System.IO
Imports GifAnimationCreator.Helpers
Imports System.ComponentModel

Public Class FrmMain

    Private Shared MimeT As New MimeType()
    Private m_InProgress As Boolean

    Private Sub Frm_Main_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ToolStripStatusLabel1.Text = "     " & OS.GetOsFullName
        TsCbxAnimationTime.Text = My.Settings.Tempo
        TsChbAnimationBorder.Checked = My.Settings.Border
    End Sub

    Private Sub Frm_Main_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If m_InProgress Then
            MessageBox.Show("Veuillez attendre la fin du processus de création !", "Création en cours", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            e.Cancel = True
        Else
            My.Settings.Tempo = TsCbxAnimationTime.Text
            My.Settings.Border = TsChbAnimationBorder.Checked
            My.Settings.Save()
        End If
    End Sub

    Private Sub TsBtnPictureAdd_Click(sender As Object, e As EventArgs) Handles TsBtnPictureAdd.Click
        Using ofd As New OpenFileDialog
            With ofd
                .Filter = "Images|*.bmp;*.gif;*.jpg;*.jpeg;*.png"
                .Title = "Sélectionnez un ou plusieurs fichiers images"
                .CheckFileExists = True
                .Multiselect = True
                If .ShowDialog() = DialogResult.OK Then
                    Dim files As String() = .FileNames
                    If Not BgwPicturesLoad.IsBusy Then
                        BgwPicturesLoad.RunWorkerAsync(files)
                    End If
                End If
            End With
        End Using
    End Sub

    Private Function MinimumSelection(num As Integer) As Boolean
        If num + LvPictures.Items.Count <= 1 Then
            MessageBox.Show("Votre liste de sélection doit contenir 2 fichiers au minimum pour créer une animation !", "Sélection insuffisante", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return True
        End If
        Return False
    End Function

    'Private Sub LvPictures_DragOver(sender As Object, e As DragEventArgs) Handles LvPictures.DragOver
    '    Dim filename As String = Nothing
    '    e.Effect = If(GetFilename(filename, e), DragDropEffects.Copy, DragDropEffects.None)

    'End Sub

    'Private Function GetFilename(ByRef filename As String, e As DragEventArgs) As Boolean
    '    Dim ret As Boolean = False
    '    filename = String.Empty
    '    If (e.AllowedEffect And DragDropEffects.Copy) = DragDropEffects.Copy Then
    '        Dim data As Array = TryCast(DirectCast(e.Data, IDataObject).GetData("FileName"), Array)
    '        If data IsNot Nothing Then
    '            If (data.Length = 1) AndAlso (TypeOf data.GetValue(0) Is String) Then
    '                filename = DirectCast(data, String())(0)
    '                Dim ext As String = Path.GetExtension(filename).ToLower()
    '                If (ext = ".bmp") OrElse (ext = ".gif") OrElse (ext = ".jpg") OrElse (ext = ".jpeg") OrElse (ext = ".png") Then
    '                    If MimeT.isCorrectMIMEType(filename) Then
    '                        ret = True
    '                    End If
    '                End If
    '            End If
    '        End If
    '    End If
    '    Return ret
    'End Function

    'Private Sub LvPictures_DragDrop(sender As Object, e As DragEventArgs) Handles LvPictures.DragDrop
    '    If (e.Data.GetDataPresent(DataFormats.FileDrop)) Then
    '        Dim strFiles() As String = e.Data.GetData(DataFormats.FileDrop)
    '        If Not Me.BgwPicturesLoad.IsBusy Then
    '            Me.BgwPicturesLoad.RunWorkerAsync(strFiles)
    '        End If
    '    End If
    'End Sub

    Private Sub BgwPicturesLoad_DoWork(sender As Object, e As DoWorkEventArgs) Handles BgwPicturesLoad.DoWork
        For Each it As String In e.Argument
            If MimeT.isCorrectMIMEType(it) Then
                Dim items As String() = New String(2) {}
                Dim fi As New FileInfo(it)
                items(0) = fi.Name
                items(1) = fi.FullName
                Dim item As New ListViewItem(items)
                item.ToolTipText = items(1)
                Dim ic As Icon = Icons.IconFromExtensionUsingRegistry(fi.Extension, Icons.SystemIconSize.Large)
                Dim obj As Object() = New Object() {item, ic}
                Me.BgwPicturesLoad.ReportProgress(Nothing, obj)
            End If
        Next
    End Sub

    Private Sub BgwPicturesLoad_ProgressChanged(sender As Object, e As ProgressChangedEventArgs) Handles BgwPicturesLoad.ProgressChanged
        Dim lvi As ListViewItem = TryCast(e.UserState(0), ListViewItem)
        Dim Ico As Icon = TryCast(e.UserState(1), Icon)
        Dim LviExists = LvPictures.Items.Cast(Of ListViewItem).Any(Function(f) f.SubItems(1).Text.ToLower = lvi.SubItems(1).Text.ToLower)
        If Not LviExists Then
            If Not Ico Is Nothing Then
                ImlPictures.Images.Add(Ico)
                lvi.ImageIndex = ImlPictures.Images.Count - 1
            End If
            LvPictures.Items.Add(lvi)
        End If
    End Sub

    Private Sub BgwPicturesLoad_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles BgwPicturesLoad.RunWorkerCompleted
        ResetDeleteButonState()
    End Sub

    Private Sub LvPictures_ItemSelectionChanged(sender As Object, ByVal e As ListViewItemSelectionChangedEventArgs) Handles LvPictures.ItemSelectionChanged
        If e.IsSelected Then
            TsBtnPictureRemove.Enabled = True
            TsBtnPictureUp.Enabled = If(e.ItemIndex = 0, False, True)
            TsBtnPictureDown.Enabled = If(e.ItemIndex = LvPictures.Items.Count - 1, False, True)
            Images.AutosizeImg(e.Item.SubItems(1).Text, Me.PcbPreview, PictureBoxSizeMode.CenterImage)
        Else
            TsBtnPictureRemove.Enabled = False
            TsBtnPictureUp.Enabled = False
            TsBtnPictureDown.Enabled = False
            PcbPreview.Image = Nothing
        End If
    End Sub

    Private Sub TsBtnPictureRemove_Click(sender As Object, e As EventArgs) Handles TsBtnPictureRemove.Click
        LvPictures.Items(LvPictures.SelectedIndices(0)).Remove()
        ResetDeleteButonState()
    End Sub

    Private Sub ResetDeleteButonState()
        Dim Bloc As Boolean = False
        If LvPictures.Items.Count > 1 Then
            TsBtnPictureEmptyList.Enabled = True
            GbxAnimation.Enabled = True
        ElseIf LvPictures.Items.Count = 1 Then
            Bloc = True
            TsBtnPictureEmptyList.Enabled = True
            MinimumSelection(0)
        Else
            Bloc = True
            TsBtnPictureEmptyList.Enabled = False
        End If
        If Bloc Then
            GbxAnimation.Enabled = False
            PcbPreview.Image = Nothing
            TsBtnPictureRemove.Enabled = False
            TsBtnPictureUp.Enabled = False
            TsBtnPictureDown.Enabled = False
            TsTxbAnimationSaveAs.Text = ""
            Bloc = False
        End If
    End Sub

    Private Sub TsBtnPictureEmptyList_Click(sender As Object, e As EventArgs) Handles TsBtnPictureEmptyList.Click
        LvPictures.Items.Clear()
        ResetDeleteButonState()
    End Sub

    Private Sub TsBtnAnimationSaveAs_Click(sender As Object, e As EventArgs) Handles TsBtnAnimationSaveAs.Click
        Using sfd As New SaveFileDialog
            With sfd
                .Filter = "GIF (*.gif)|*.gif"
                .Title = "Enregistrer votre fichier sous"
                .OverwritePrompt = True
                .InitialDirectory = My.Computer.FileSystem.SpecialDirectories.MyPictures
                If .ShowDialog() = DialogResult.OK Then
                    Dim LviExists = LvPictures.Items.Cast(Of ListViewItem).Any(Function(f) f.SubItems(1).Text.ToLower = .FileName.ToLower)
                    If Not LviExists Then
                        TsTxbAnimationSaveAs.Text = .FileName
                        BtnAnimationStart.Enabled = If(Not TsTxbAnimationSaveAs.Text.Length <> 0, False, True)
                    Else
                        MessageBox.Show("Ce fichier est présent dans votre liste de sélection, par conséquent il ne peut être sélectionné en tant que fichier de sauvegarde !", "Mauvaise destination", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    End If
                End If
            End With
        End Using
    End Sub

    Private Sub BtnAnimationStart_Click(sender As Object, e As EventArgs) Handles BtnAnimationStart.Click
        If Not BgwAnimationStart.IsBusy Then
            GbxPictures.Enabled = False
            GbxPreview.Enabled = False
            TsAnimation.Enabled = False
            PgbAnimationProgress.Value = 0

            Dim Widths As New List(Of Integer)
            Dim Heights As New List(Of Integer)

            Dim str As New List(Of String)
            For Each lvi As ListViewItem In LvPictures.Items
                str.Add(lvi.SubItems(1).Text)
                Dim img = Image.FromFile(lvi.SubItems(1).Text)
                Widths.Add(img.Width)
                Heights.Add(img.Height)
                img.Dispose()
            Next

            Dim sizes = New Size(Widths.Max(), Heights.Max())

            BtnAnimationStart.Text = "Annuler la création"

            Dim obj As Object() = New Object() {str, CInt(TsCbxAnimationTime.SelectedItem), sizes, TsChbAnimationBorder.Checked}
            BgwAnimationStart.RunWorkerAsync(obj)
        Else
            BgwAnimationStart.CancelAsync()
        End If
    End Sub

    Private Sub BgwAnimationStart_DoWork(sender As Object, e As DoWorkEventArgs) Handles BgwAnimationStart.DoWork
        Try
            Dim borderSize = 0
            Dim sizes = e.Argument(2)
            Dim border = CBool(e.Argument(3))
            If border Then borderSize = 1

            Dim i As Integer = 0
            m_InProgress = True
            BgwAnimationStart.ReportProgress(200, "Création de l'animation ...")
            Dim lvi = TryCast(e.Argument(0), List(Of String))
            Dim outputFilePath = TsTxbAnimationSaveAs.Text
            Dim ag As New AnimatedGifEncoder()
            'ag.SetQuality(100)
            ag.Start(outputFilePath)
            ag.SetDelay(e.Argument(1))
            '-1:no repeat,0:always repeat
            ag.SetRepeat(0)
            Dim count As Integer = lvi.Count
            While i < count
                If BgwAnimationStart.CancellationPending Then
                    e.Cancel = True
                    Exit Sub
                End If
                Dim fi As New FileInfo(lvi(i))
                Dim img = Image.FromFile(fi.FullName)
                If border Then
                    img = Images.DrawBitmapWithBorder(img, borderSize)
                End If
                Dim scale = Images.ScaleImage(img, sizes.Width + borderSize, sizes.Height + borderSize)
                'ag.SetTransparent(Color.Transparent)
                ag.AddFrame(scale)
                img.Dispose()
                scale.Dispose()
                i += 1
                BgwAnimationStart.ReportProgress((i * 100) / count, i - 1)
            End While

            ag.Finish()
            e.Result = True
        Catch ex As Exception
            e.Result = False
        End Try
    End Sub

    Private Sub BgwAnimationStart_ProgressChanged(sender As Object, e As ProgressChangedEventArgs) Handles BgwAnimationStart.ProgressChanged
        If e.ProgressPercentage = 200 Then
            PgbAnimationProgress.Value = 0
            Text = e.UserState.ToString
        Else
            If Not e.ProgressPercentage > 100 Then
                PgbAnimationProgress.Value = e.ProgressPercentage
                LvPictures.Items(CInt(e.UserState)).Selected = True
            End If
        End If
    End Sub

    Private Sub BgwAnimationStart_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles BgwAnimationStart.RunWorkerCompleted
        GbxPictures.Enabled = True
        GbxPreview.Enabled = True
        TsAnimation.Enabled = True
        BtnAnimationStart.Enabled = True
        PgbAnimationProgress.Value = 100
        m_InProgress = False
        Text = "Gif Animation Creator"
        BtnAnimationStart.Text = "Lancer la création"

        If e.Cancelled Then
            ThreadAnimAborted()
        Else
            If e.Result = True Then
                ThreadAnimCreated()
            Else
                ThreadAnimNotCreated()
            End If
        End If
    End Sub

    Private Sub TsBtnPictureUp_Click(sender As Object, e As EventArgs) Handles TsBtnPictureUp.Click
        MoveListViewItem(LvPictures, True)
    End Sub

    Private Sub MoveListViewItem(lv As ListView, moveUp As Boolean)
        Dim i As Integer
        Dim cache As String
        Dim selIdx As Integer

        With lv
            selIdx = .SelectedItems.Item(0).Index
            If moveUp Then
                ' ignore moveup of row(0)
                If selIdx = 0 Then
                    Exit Sub
                End If
                ' move the subitems for the previous row
                ' to cache so we can move the selected row up
                For i = 0 To .Items(selIdx).SubItems.Count - 1
                    cache = .Items(selIdx - 1).SubItems(i).Text
                    .Items(selIdx - 1).SubItems(i).Text =
                       .Items(selIdx).SubItems(i).Text
                    .Items(selIdx).SubItems(i).Text = cache
                Next
                .Items(selIdx - 1).Selected = True
                .Refresh()
                .Focus()
                If selIdx = 1 Then
                    TsBtnPictureUp.Enabled = False
                End If
            Else
                ' ignore move down of last row
                If selIdx = .Items.Count - 1 Then
                    Exit Sub
                End If
                ' move the subitems for the next row
                ' to cache so we can move the selected row down
                For i = 0 To .Items(selIdx).SubItems.Count - 1
                    cache = .Items(selIdx + 1).SubItems(i).Text
                    .Items(selIdx + 1).SubItems(i).Text =
                       .Items(selIdx).SubItems(i).Text
                    .Items(selIdx).SubItems(i).Text = cache
                Next
                .Items(selIdx + 1).Selected = True
                .Refresh()
                .Focus()
                If selIdx = .Items.Count - 2 Then
                    TsBtnPictureDown.Enabled = False
                End If
            End If
        End With
    End Sub

    Private Sub TsBtnPictureDown_Click(sender As Object, e As EventArgs) Handles TsBtnPictureDown.Click
        MoveListViewItem(LvPictures, False)
    End Sub

    Private Sub ThreadAnimCreated()
        Using dl2 As New FrmInfo("Création de l'animation achevée.", FrmInfo.InfoStates.Info)
            dl2.ShowDialog()
        End Using
    End Sub

    Private Sub ThreadAnimNotCreated()
        Using dl2 As New FrmInfo("Echec lors de la création de l'animation !", FrmInfo.InfoStates.Error)
            dl2.ShowDialog()
        End Using
    End Sub

    Private Sub ThreadAnimAborted()
        Using dl2 As New FrmInfo("La création de l'animation a été annulée.", FrmInfo.InfoStates.Warning)
            dl2.ShowDialog()
        End Using
    End Sub

    Private Sub TsChbAnimationBorder_CheckedChanged(sender As Object, e As EventArgs) Handles TsChbAnimationBorder.CheckedChanged
        My.Settings.Border = TsChbAnimationBorder.Checked
        My.Settings.Save()
    End Sub

End Class