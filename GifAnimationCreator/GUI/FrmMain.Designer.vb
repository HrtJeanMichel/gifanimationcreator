﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmMain
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmMain))
        Me.GbxPictures = New System.Windows.Forms.GroupBox()
        Me.TsPictures = New System.Windows.Forms.ToolStrip()
        Me.ToolStripLabel9 = New System.Windows.Forms.ToolStripLabel()
        Me.TsBtnPictureAdd = New System.Windows.Forms.ToolStripButton()
        Me.TsBtnPictureRemove = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripLabel10 = New System.Windows.Forms.ToolStripLabel()
        Me.ToolStripLabel2 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripLabel11 = New System.Windows.Forms.ToolStripLabel()
        Me.TsBtnPictureUp = New System.Windows.Forms.ToolStripButton()
        Me.TsBtnPictureDown = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripLabel12 = New System.Windows.Forms.ToolStripLabel()
        Me.ToolStripLabel3 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripLabel1 = New System.Windows.Forms.ToolStripLabel()
        Me.TsBtnPictureEmptyList = New System.Windows.Forms.ToolStripButton()
        Me.ImlPictures = New System.Windows.Forms.ImageList(Me.components)
        Me.GbxPreview = New System.Windows.Forms.GroupBox()
        Me.PcbPreview = New System.Windows.Forms.PictureBox()
        Me.BgwPicturesLoad = New System.ComponentModel.BackgroundWorker()
        Me.GbxAnimation = New System.Windows.Forms.GroupBox()
        Me.TsChbAnimationBorder = New System.Windows.Forms.CheckBox()
        Me.BtnAnimationStart = New System.Windows.Forms.Button()
        Me.TsAnimation = New System.Windows.Forms.ToolStrip()
        Me.ToolStripLabel8 = New System.Windows.Forms.ToolStripLabel()
        Me.TsLblAnimation = New System.Windows.Forms.ToolStripLabel()
        Me.ToolStripLabel7 = New System.Windows.Forms.ToolStripLabel()
        Me.TsCbxAnimationTime = New System.Windows.Forms.ToolStripComboBox()
        Me.ToolStripLabel5 = New System.Windows.Forms.ToolStripLabel()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStripLabel6 = New System.Windows.Forms.ToolStripLabel()
        Me.TsBtnAnimationSaveAs = New System.Windows.Forms.ToolStripButton()
        Me.TsTxbAnimationSaveAs = New System.Windows.Forms.ToolStripTextBox()
        Me.ToolStripLabel17 = New System.Windows.Forms.ToolStripLabel()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.SsInfos = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.BgwAnimationStart = New System.ComponentModel.BackgroundWorker()
        Me.PgbAnimationProgress = New GifAnimationCreator.Extensions.ProgressBarEx()
        Me.LvPictures = New GifAnimationCreator.Extensions.ListViewEx()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.GbxPictures.SuspendLayout()
        Me.TsPictures.SuspendLayout()
        Me.GbxPreview.SuspendLayout()
        CType(Me.PcbPreview, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GbxAnimation.SuspendLayout()
        Me.TsAnimation.SuspendLayout()
        Me.SsInfos.SuspendLayout()
        Me.SuspendLayout()
        '
        'GbxPictures
        '
        Me.GbxPictures.Controls.Add(Me.TsPictures)
        Me.GbxPictures.Controls.Add(Me.LvPictures)
        Me.GbxPictures.Location = New System.Drawing.Point(12, 12)
        Me.GbxPictures.Name = "GbxPictures"
        Me.GbxPictures.Size = New System.Drawing.Size(208, 637)
        Me.GbxPictures.TabIndex = 1
        Me.GbxPictures.TabStop = False
        Me.GbxPictures.Text = "Images sélectionnées"
        '
        'TsPictures
        '
        Me.TsPictures.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.TsPictures.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripLabel9, Me.TsBtnPictureAdd, Me.TsBtnPictureRemove, Me.ToolStripLabel10, Me.ToolStripLabel2, Me.ToolStripLabel11, Me.TsBtnPictureUp, Me.TsBtnPictureDown, Me.ToolStripLabel12, Me.ToolStripLabel3, Me.ToolStripLabel1, Me.TsBtnPictureEmptyList})
        Me.TsPictures.Location = New System.Drawing.Point(3, 16)
        Me.TsPictures.Name = "TsPictures"
        Me.TsPictures.Size = New System.Drawing.Size(202, 25)
        Me.TsPictures.TabIndex = 1
        Me.TsPictures.Text = "ToolStrip1"
        '
        'ToolStripLabel9
        '
        Me.ToolStripLabel9.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripLabel9.Enabled = False
        Me.ToolStripLabel9.Image = CType(resources.GetObject("ToolStripLabel9.Image"), System.Drawing.Image)
        Me.ToolStripLabel9.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripLabel9.Name = "ToolStripLabel9"
        Me.ToolStripLabel9.Size = New System.Drawing.Size(16, 22)
        Me.ToolStripLabel9.Text = "   "
        '
        'TsBtnPictureAdd
        '
        Me.TsBtnPictureAdd.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.TsBtnPictureAdd.Image = Global.GifAnimationCreator.My.Resources.Resources.Add
        Me.TsBtnPictureAdd.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TsBtnPictureAdd.Name = "TsBtnPictureAdd"
        Me.TsBtnPictureAdd.Size = New System.Drawing.Size(23, 22)
        Me.TsBtnPictureAdd.Text = "Ajouter à la liste"
        '
        'TsBtnPictureRemove
        '
        Me.TsBtnPictureRemove.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.TsBtnPictureRemove.Enabled = False
        Me.TsBtnPictureRemove.Image = Global.GifAnimationCreator.My.Resources.Resources.less
        Me.TsBtnPictureRemove.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TsBtnPictureRemove.Name = "TsBtnPictureRemove"
        Me.TsBtnPictureRemove.Size = New System.Drawing.Size(23, 22)
        Me.TsBtnPictureRemove.Text = "Ôter de la liste"
        '
        'ToolStripLabel10
        '
        Me.ToolStripLabel10.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripLabel10.Enabled = False
        Me.ToolStripLabel10.Image = CType(resources.GetObject("ToolStripLabel10.Image"), System.Drawing.Image)
        Me.ToolStripLabel10.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripLabel10.Name = "ToolStripLabel10"
        Me.ToolStripLabel10.Size = New System.Drawing.Size(10, 22)
        Me.ToolStripLabel10.Text = " "
        '
        'ToolStripLabel2
        '
        Me.ToolStripLabel2.Name = "ToolStripLabel2"
        Me.ToolStripLabel2.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripLabel11
        '
        Me.ToolStripLabel11.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripLabel11.Enabled = False
        Me.ToolStripLabel11.Image = CType(resources.GetObject("ToolStripLabel11.Image"), System.Drawing.Image)
        Me.ToolStripLabel11.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripLabel11.Name = "ToolStripLabel11"
        Me.ToolStripLabel11.Size = New System.Drawing.Size(10, 22)
        Me.ToolStripLabel11.Text = " "
        '
        'TsBtnPictureUp
        '
        Me.TsBtnPictureUp.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.TsBtnPictureUp.Enabled = False
        Me.TsBtnPictureUp.Image = Global.GifAnimationCreator.My.Resources.Resources.Up
        Me.TsBtnPictureUp.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TsBtnPictureUp.Name = "TsBtnPictureUp"
        Me.TsBtnPictureUp.Size = New System.Drawing.Size(23, 22)
        Me.TsBtnPictureUp.Text = "Déplacer en haut"
        '
        'TsBtnPictureDown
        '
        Me.TsBtnPictureDown.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.TsBtnPictureDown.Enabled = False
        Me.TsBtnPictureDown.Image = Global.GifAnimationCreator.My.Resources.Resources.Down
        Me.TsBtnPictureDown.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TsBtnPictureDown.Name = "TsBtnPictureDown"
        Me.TsBtnPictureDown.Size = New System.Drawing.Size(23, 22)
        Me.TsBtnPictureDown.Text = "Déplacer en bas"
        '
        'ToolStripLabel12
        '
        Me.ToolStripLabel12.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripLabel12.Enabled = False
        Me.ToolStripLabel12.Image = CType(resources.GetObject("ToolStripLabel12.Image"), System.Drawing.Image)
        Me.ToolStripLabel12.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripLabel12.Name = "ToolStripLabel12"
        Me.ToolStripLabel12.Size = New System.Drawing.Size(10, 22)
        Me.ToolStripLabel12.Text = " "
        '
        'ToolStripLabel3
        '
        Me.ToolStripLabel3.Name = "ToolStripLabel3"
        Me.ToolStripLabel3.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripLabel1
        '
        Me.ToolStripLabel1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripLabel1.Enabled = False
        Me.ToolStripLabel1.Image = CType(resources.GetObject("ToolStripLabel1.Image"), System.Drawing.Image)
        Me.ToolStripLabel1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripLabel1.Name = "ToolStripLabel1"
        Me.ToolStripLabel1.Size = New System.Drawing.Size(10, 22)
        Me.ToolStripLabel1.Text = " "
        '
        'TsBtnPictureEmptyList
        '
        Me.TsBtnPictureEmptyList.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.TsBtnPictureEmptyList.Enabled = False
        Me.TsBtnPictureEmptyList.Image = Global.GifAnimationCreator.My.Resources.Resources.Del
        Me.TsBtnPictureEmptyList.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TsBtnPictureEmptyList.Name = "TsBtnPictureEmptyList"
        Me.TsBtnPictureEmptyList.Size = New System.Drawing.Size(23, 22)
        Me.TsBtnPictureEmptyList.Text = "Vider la liste"
        '
        'ImlPictures
        '
        Me.ImlPictures.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit
        Me.ImlPictures.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImlPictures.TransparentColor = System.Drawing.Color.Transparent
        '
        'GbxPreview
        '
        Me.GbxPreview.Controls.Add(Me.PcbPreview)
        Me.GbxPreview.Location = New System.Drawing.Point(226, 12)
        Me.GbxPreview.Name = "GbxPreview"
        Me.GbxPreview.Size = New System.Drawing.Size(1008, 529)
        Me.GbxPreview.TabIndex = 3
        Me.GbxPreview.TabStop = False
        Me.GbxPreview.Text = "Aperçu"
        '
        'PcbPreview
        '
        Me.PcbPreview.Location = New System.Drawing.Point(6, 19)
        Me.PcbPreview.Name = "PcbPreview"
        Me.PcbPreview.Size = New System.Drawing.Size(996, 504)
        Me.PcbPreview.TabIndex = 2
        Me.PcbPreview.TabStop = False
        '
        'BgwPicturesLoad
        '
        Me.BgwPicturesLoad.WorkerReportsProgress = True
        '
        'GbxAnimation
        '
        Me.GbxAnimation.Controls.Add(Me.TsChbAnimationBorder)
        Me.GbxAnimation.Controls.Add(Me.BtnAnimationStart)
        Me.GbxAnimation.Controls.Add(Me.PgbAnimationProgress)
        Me.GbxAnimation.Controls.Add(Me.TsAnimation)
        Me.GbxAnimation.Enabled = False
        Me.GbxAnimation.Location = New System.Drawing.Point(232, 547)
        Me.GbxAnimation.Name = "GbxAnimation"
        Me.GbxAnimation.Size = New System.Drawing.Size(1002, 102)
        Me.GbxAnimation.TabIndex = 4
        Me.GbxAnimation.TabStop = False
        Me.GbxAnimation.Text = "Création de l'animation"
        '
        'TsChbAnimationBorder
        '
        Me.TsChbAnimationBorder.AutoSize = True
        Me.TsChbAnimationBorder.BackColor = System.Drawing.Color.Transparent
        Me.TsChbAnimationBorder.Checked = True
        Me.TsChbAnimationBorder.CheckState = System.Windows.Forms.CheckState.Checked
        Me.TsChbAnimationBorder.Location = New System.Drawing.Point(896, 20)
        Me.TsChbAnimationBorder.Name = "TsChbAnimationBorder"
        Me.TsChbAnimationBorder.Size = New System.Drawing.Size(98, 17)
        Me.TsChbAnimationBorder.TabIndex = 8
        Me.TsChbAnimationBorder.Text = "Ajouter contour"
        Me.TsChbAnimationBorder.UseVisualStyleBackColor = False
        '
        'BtnAnimationStart
        '
        Me.BtnAnimationStart.Enabled = False
        Me.BtnAnimationStart.Location = New System.Drawing.Point(3, 44)
        Me.BtnAnimationStart.Name = "BtnAnimationStart"
        Me.BtnAnimationStart.Size = New System.Drawing.Size(996, 26)
        Me.BtnAnimationStart.TabIndex = 5
        Me.BtnAnimationStart.Text = "Lancer la création"
        Me.BtnAnimationStart.UseVisualStyleBackColor = True
        '
        'TsAnimation
        '
        Me.TsAnimation.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.TsAnimation.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripLabel8, Me.TsLblAnimation, Me.ToolStripLabel7, Me.TsCbxAnimationTime, Me.ToolStripLabel5, Me.ToolStripSeparator1, Me.ToolStripLabel6, Me.TsBtnAnimationSaveAs, Me.TsTxbAnimationSaveAs, Me.ToolStripLabel17, Me.ToolStripSeparator2})
        Me.TsAnimation.Location = New System.Drawing.Point(3, 16)
        Me.TsAnimation.Name = "TsAnimation"
        Me.TsAnimation.Size = New System.Drawing.Size(996, 25)
        Me.TsAnimation.TabIndex = 6
        Me.TsAnimation.Text = "ToolStrip2"
        '
        'ToolStripLabel8
        '
        Me.ToolStripLabel8.Name = "ToolStripLabel8"
        Me.ToolStripLabel8.Size = New System.Drawing.Size(22, 22)
        Me.ToolStripLabel8.Text = "     "
        '
        'TsLblAnimation
        '
        Me.TsLblAnimation.Image = Global.GifAnimationCreator.My.Resources.Resources.hourglass
        Me.TsLblAnimation.Name = "TsLblAnimation"
        Me.TsLblAnimation.Size = New System.Drawing.Size(16, 22)
        Me.TsLblAnimation.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.TsLblAnimation.ToolTipText = "Temporisation"
        '
        'ToolStripLabel7
        '
        Me.ToolStripLabel7.Name = "ToolStripLabel7"
        Me.ToolStripLabel7.Size = New System.Drawing.Size(10, 22)
        Me.ToolStripLabel7.Text = " "
        '
        'TsCbxAnimationTime
        '
        Me.TsCbxAnimationTime.AutoSize = False
        Me.TsCbxAnimationTime.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.TsCbxAnimationTime.FlatStyle = System.Windows.Forms.FlatStyle.Standard
        Me.TsCbxAnimationTime.Items.AddRange(New Object() {"200", "300", "400", "500", "600", "700", "800", "900", "1000", "1200", "1400", "1600", "1800", "2000", "2200", "2400", "2600", "2800", "3000", "3200", "3400", "3600", "3800", "4000", "5000", "6000", "7000", "8000", "9000", "10000"})
        Me.TsCbxAnimationTime.Name = "TsCbxAnimationTime"
        Me.TsCbxAnimationTime.Size = New System.Drawing.Size(60, 23)
        Me.TsCbxAnimationTime.ToolTipText = "Temporisation (en millisecondes)"
        '
        'ToolStripLabel5
        '
        Me.ToolStripLabel5.Name = "ToolStripLabel5"
        Me.ToolStripLabel5.Size = New System.Drawing.Size(13, 22)
        Me.ToolStripLabel5.Text = "  "
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'ToolStripLabel6
        '
        Me.ToolStripLabel6.Name = "ToolStripLabel6"
        Me.ToolStripLabel6.Size = New System.Drawing.Size(10, 22)
        Me.ToolStripLabel6.Text = " "
        '
        'TsBtnAnimationSaveAs
        '
        Me.TsBtnAnimationSaveAs.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.TsBtnAnimationSaveAs.Image = CType(resources.GetObject("TsBtnAnimationSaveAs.Image"), System.Drawing.Image)
        Me.TsBtnAnimationSaveAs.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.TsBtnAnimationSaveAs.Name = "TsBtnAnimationSaveAs"
        Me.TsBtnAnimationSaveAs.Size = New System.Drawing.Size(23, 22)
        Me.TsBtnAnimationSaveAs.Text = "..."
        Me.TsBtnAnimationSaveAs.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.TsBtnAnimationSaveAs.ToolTipText = "Parcourir"
        '
        'TsTxbAnimationSaveAs
        '
        Me.TsTxbAnimationSaveAs.BackColor = System.Drawing.Color.White
        Me.TsTxbAnimationSaveAs.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TsTxbAnimationSaveAs.Name = "TsTxbAnimationSaveAs"
        Me.TsTxbAnimationSaveAs.ReadOnly = True
        Me.TsTxbAnimationSaveAs.Size = New System.Drawing.Size(720, 25)
        Me.TsTxbAnimationSaveAs.ToolTipText = "Chemin du ficher de sauvegarde"
        '
        'ToolStripLabel17
        '
        Me.ToolStripLabel17.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ToolStripLabel17.Name = "ToolStripLabel17"
        Me.ToolStripLabel17.Size = New System.Drawing.Size(13, 22)
        Me.ToolStripLabel17.Text = "  "
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'SsInfos
        '
        Me.SsInfos.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1})
        Me.SsInfos.Location = New System.Drawing.Point(0, 652)
        Me.SsInfos.Name = "SsInfos"
        Me.SsInfos.Size = New System.Drawing.Size(1246, 22)
        Me.SsInfos.TabIndex = 5
        Me.SsInfos.Text = "StatusStrip1"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(0, 17)
        '
        'BgwAnimationStart
        '
        Me.BgwAnimationStart.WorkerReportsProgress = True
        Me.BgwAnimationStart.WorkerSupportsCancellation = True
        '
        'PgbAnimationProgress
        '
        Me.PgbAnimationProgress.ForeColor = System.Drawing.Color.DimGray
        Me.PgbAnimationProgress.Location = New System.Drawing.Point(6, 74)
        Me.PgbAnimationProgress.Name = "PgbAnimationProgress"
        Me.PgbAnimationProgress.OverLayColor = System.Drawing.Color.Black
        Me.PgbAnimationProgress.Percentage = 0R
        Me.PgbAnimationProgress.PercentageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.PgbAnimationProgress.Size = New System.Drawing.Size(990, 22)
        Me.PgbAnimationProgress.TabIndex = 7
        '
        'LvPictures
        '
        Me.LvPictures.AllowDrop = True
        Me.LvPictures.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.LvPictures.FullRowSelect = True
        Me.LvPictures.GridLines = True
        Me.LvPictures.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.LvPictures.HideSelection = False
        Me.LvPictures.InsertionLineColor = System.Drawing.Color.DeepSkyBlue
        Me.LvPictures.LargeImageList = Me.ImlPictures
        Me.LvPictures.Location = New System.Drawing.Point(6, 41)
        Me.LvPictures.MultiSelect = False
        Me.LvPictures.Name = "LvPictures"
        Me.LvPictures.ShowItemToolTips = True
        Me.LvPictures.Size = New System.Drawing.Size(196, 590)
        Me.LvPictures.SmallImageList = Me.ImlPictures
        Me.LvPictures.TabIndex = 0
        Me.LvPictures.UseCompatibleStateImageBehavior = False
        Me.LvPictures.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Nom du fichier"
        Me.ColumnHeader1.Width = 190
        '
        'FrmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1246, 674)
        Me.Controls.Add(Me.SsInfos)
        Me.Controls.Add(Me.GbxAnimation)
        Me.Controls.Add(Me.GbxPreview)
        Me.Controls.Add(Me.GbxPictures)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "FrmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Gif Animation Creator"
        Me.GbxPictures.ResumeLayout(False)
        Me.GbxPictures.PerformLayout()
        Me.TsPictures.ResumeLayout(False)
        Me.TsPictures.PerformLayout()
        Me.GbxPreview.ResumeLayout(False)
        CType(Me.PcbPreview, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GbxAnimation.ResumeLayout(False)
        Me.GbxAnimation.PerformLayout()
        Me.TsAnimation.ResumeLayout(False)
        Me.TsAnimation.PerformLayout()
        Me.SsInfos.ResumeLayout(False)
        Me.SsInfos.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LvPictures As Extensions.ListViewEx
    Friend WithEvents GbxPictures As System.Windows.Forms.GroupBox
    Friend WithEvents TsPictures As System.Windows.Forms.ToolStrip
    Friend WithEvents TsBtnPictureAdd As System.Windows.Forms.ToolStripButton
    Friend WithEvents TsBtnPictureUp As System.Windows.Forms.ToolStripButton
    Friend WithEvents TsBtnPictureDown As System.Windows.Forms.ToolStripButton
    Friend WithEvents PcbPreview As System.Windows.Forms.PictureBox
    Friend WithEvents GbxPreview As System.Windows.Forms.GroupBox
    Friend WithEvents BgwPicturesLoad As System.ComponentModel.BackgroundWorker
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents TsBtnPictureRemove As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripLabel1 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents TsBtnPictureEmptyList As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripLabel2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripLabel3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents GbxAnimation As System.Windows.Forms.GroupBox
    Friend WithEvents BtnAnimationStart As System.Windows.Forms.Button
    Friend WithEvents TsAnimation As System.Windows.Forms.ToolStrip
    Friend WithEvents TsLblAnimation As System.Windows.Forms.ToolStripLabel
    Friend WithEvents TsCbxAnimationTime As System.Windows.Forms.ToolStripComboBox
    Friend WithEvents TsTxbAnimationSaveAs As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents TsBtnAnimationSaveAs As System.Windows.Forms.ToolStripButton
    Friend WithEvents PgbAnimationProgress As Extensions.ProgressBarEx
    Friend WithEvents ImlPictures As System.Windows.Forms.ImageList
    Friend WithEvents SsInfos As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents BgwAnimationStart As System.ComponentModel.BackgroundWorker
    Friend WithEvents ToolStripLabel8 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolStripLabel9 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolStripLabel10 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolStripLabel11 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolStripLabel12 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolStripLabel5 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripLabel6 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolStripLabel7 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents ToolStripLabel17 As System.Windows.Forms.ToolStripLabel
    Friend WithEvents TsChbAnimationBorder As CheckBox
    Friend WithEvents ToolStripSeparator2 As ToolStripSeparator
End Class
