﻿Imports System.IO

Namespace Helpers
    Public Class OS

#Region " Fields "
        Private Shared m_ProgramFiles32 As String = "C:\Program Files (x86)"
#End Region

#Region " Methods "
        ''' <summary>
        ''' Retourne la valeur chaine du système d'exploitation et sa version.
        ''' </summary>
        ''' <returns>Valeur de type String</returns>
        Private Shared Function GetOsProductName() As String
            Dim ProdName = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion", "ProductName", Nothing).ToString()
            Return ProdName.Trim
        End Function

        Public Shared Function GetOsFullName() As String
            Return GetOsProductName() & " " & GetReleaseId() & " " & "(" & If(is64Bits(), "x64", "x32") & "bits)"
        End Function

        ''' <summary>
        ''' Retourne le numéro de Build du système d'exploitation.
        ''' </summary>
        ''' <returns>Valeur de type String</returns>
        Private Shared Function GetReleaseId() As String
            Dim Realeaseid = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion", "ReleaseId", "").ToString()
            Return Realeaseid.Trim
        End Function

        ''' <summary>
        ''' Renvoie la version du système d'exploitation (Windows Vista, Windows 7, Windows 8, Windows 8.1, Windows 10, ...
        ''' </summary>
        ''' <returns>Valeur de type String</returns>
        Public Shared Function GetOsVersion() As String
            Dim oSVersion As OperatingSystem = Environment.OSVersion
            Select Case oSVersion.Platform.ToString
                Case "Win32Windows"
                    Select Case oSVersion.Version.Minor
                        Case 0
                            Return "Windows 95"
                        Case 10
                            If (oSVersion.Version.Revision.ToString = "2222A") Then
                                Return "Windows 98 Second Edition"
                            End If
                            Return "Windows 98"
                        Case 90
                            Return "Windows Me"
                    End Select
                    Exit Select
                Case "Win32NT"
                    Select Case oSVersion.Version.Major
                        Case 3
                            Return "Windows NT 3.51"
                        Case 4
                            Return "Windows NT 4.0"
                        Case 5
                            Select Case oSVersion.Version.Minor
                                Case 0
                                    Return "Windows 2000"
                                Case 1
                                    Return "Windows XP"
                                Case 2
                                    If My.Computer.Info.OSFullName.Contains("R2") Then
                                        Return "Windows Server 2003 R2"
                                    Else
                                        Return "Windows Server 2003"
                                    End If
                            End Select
                            Exit Select
                        Case 6
                            Select Case oSVersion.Version.Minor
                                Case 0
                                    If My.Computer.Info.OSFullName.Contains("Serv") Then
                                        Return "Windows Server 2008"
                                    ElseIf My.Computer.Info.OSFullName.Contains("Vista") Then
                                        Return "Windows Vista"
                                    ElseIf My.Computer.Info.OSFullName.Contains("Embedded") Then
                                        Return "Windows Embedded"
                                    End If
                                Case 1
                                    If My.Computer.Info.OSFullName.Contains("R2") Then
                                        Return "Windows Server 2008 R2"
                                    ElseIf My.Computer.Info.OSFullName.Contains("7") Then
                                        Return "Windows 7"
                                    ElseIf My.Computer.Info.OSFullName.Contains("Embedded") Then
                                        Return "Windows Embedded"
                                    End If
                                Case 2
                                    If My.Computer.Info.OSFullName.Contains("Serv") Then
                                        Return "Windows Server 2012"
                                    ElseIf My.Computer.Info.OSFullName.Contains("8") Then
                                        Return "Windows 8"
                                    ElseIf My.Computer.Info.OSFullName.Contains("Embedded") Then
                                        Return "Windows Embedded"
                                    ElseIf My.Computer.Info.OSFullName.Contains("10") Then
                                        Return "Windows 10"
                                    End If
                                Case 3
                                    If My.Computer.Info.OSFullName.Contains("Serv") Then
                                        Return "Windows Server 2012 R2"
                                    ElseIf My.Computer.Info.OSFullName.Contains("8.1") Then
                                        Return "Windows 8.1"
                                    ElseIf My.Computer.Info.OSFullName.Contains("Embedded") Then
                                        Return "Windows Embedded"
                                    ElseIf My.Computer.Info.OSFullName.Contains("10") Then
                                        Return "Windows 10"
                                    End If
                                Case 4
                                    If My.Computer.Info.OSFullName.Contains("10") Then
                                        Return "Windows 10"
                                    ElseIf My.Computer.Info.OSFullName.Contains("Embedded") Then
                                        Return "Windows Embedded"
                                    End If
                            End Select
                        Case 10
                            If My.Computer.Info.OSFullName.Contains("10") Then
                                Return "Windows 10"
                            ElseIf My.Computer.Info.OSFullName.Contains("Embedded") Then
                                Return "Windows Embedded"
                            End If
                            Exit Select
                    End Select
                    Exit Select
            End Select
            Return "Système d'exploitation inconnu !"
        End Function

        '''' <summary>
        '''' Détermine l'architecture du système d'exploitation en se basant sur l'existence du chemin "C:\Program Files (x86)".
        '''' </summary>
        Public Shared Function is64Bits() As Boolean
            Return Directory.Exists(m_ProgramFiles32)
        End Function
#End Region

    End Class
End Namespace


