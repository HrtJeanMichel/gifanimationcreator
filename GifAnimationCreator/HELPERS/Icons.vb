﻿' * 
' * Copyright (C) 2006-2010 Julien Roncaglia
' *
' * This library is free software; you can redistribute it and/or
' * modify it under the terms of the GNU Lesser General Public
' * License as published by the Free Software Foundation; either
' * version 2.1 of the License, or (at your option) any later version.
' *
' * This library is distributed in the hope that it will be useful,
' * but WITHOUT ANY WARRANTY; without even the implied warranty of
' * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
' * Lesser General Public License for more details.
' *
' * You should have received a copy of the GNU Lesser General Public
' * License along with this library; if not, write to the Free Software
' * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
'

Imports System.Reflection
Imports System.Runtime.InteropServices
Imports GifAnimationCreator.Win32
Imports Microsoft.Win32

Namespace Helpers

    Public NotInheritable Class Icons
        Private Sub New()
        End Sub

#Region "Custom exceptions class"

        Public Class IconNotFoundException
            Inherits Exception
            Public Sub New(fileName As String, index As Integer, innerException As Exception)
                MyBase.New(String.Format("Id = {0} introuvable dans le fichier {1}", index, fileName), innerException)
            End Sub
        End Class

        Public Class UnableToExtractIconsException
            Inherits Exception
            Public Sub New(fileName As String, firstIconIndex As Integer, iconCount As Integer)
                MyBase.New(String.Format("La tentative d'extraction de {2} avec l'id {1} depuis le fichier ""{0}"" a échouée !", fileName, firstIconIndex, iconCount))
            End Sub
        End Class

#End Region

        ''' <summary>
        ''' Two constants extracted from the FileInfoFlags, the only that are
        ''' meaningfull for the user of this class.
        ''' </summary>
        Public Enum SystemIconSize As Integer
            Large = 0
            Small = 1
        End Enum

        ''' <summary>
        ''' Get the number of icons in the specified file.
        ''' </summary>
        ''' <param name="fileName">Full path of the file to look for.</param>
        ''' <returns></returns>
        Private Shared Function GetIconsCountInFile(fileName As String) As Integer
            Return NativeMethods.ExtractIconEx(fileName, -1, Nothing, Nothing, 0)
        End Function

#Region "ExtractIcon-like functions"

        Public Shared Sub ExtractEx(fileName As String, largeIcons As List(Of Icon), smallIcons As List(Of Icon), firstIconIndex As Integer, iconCount As Integer)
            '
            '             * Memory allocations
            '             


            Dim smallIconsPtrs As IntPtr() = Nothing
            Dim largeIconsPtrs As IntPtr() = Nothing

            If smallIcons IsNot Nothing Then
                smallIconsPtrs = New IntPtr(iconCount - 1) {}
            End If
            If largeIcons IsNot Nothing Then
                largeIconsPtrs = New IntPtr(iconCount - 1) {}
            End If

            '
            '             * Call to native Win32 API
            '             


            Dim apiResult As Integer = NativeMethods.ExtractIconEx(fileName, firstIconIndex, largeIconsPtrs, smallIconsPtrs, iconCount)
            If apiResult <> iconCount Then
                Throw New UnableToExtractIconsException(fileName, firstIconIndex, iconCount)
            End If

            '
            '             * Fill lists
            '             


            If smallIcons IsNot Nothing Then
                smallIcons.Clear()
                For Each actualIconPtr As IntPtr In smallIconsPtrs
                    smallIcons.Add(Icon.FromHandle(actualIconPtr))
                Next
            End If
            If largeIcons IsNot Nothing Then
                largeIcons.Clear()
                For Each actualIconPtr As IntPtr In largeIconsPtrs
                    largeIcons.Add(Icon.FromHandle(actualIconPtr))
                Next
            End If
        End Sub

        Public Shared Function ExtractEx(fileName As String, size As SystemIconSize, firstIconIndex As Integer, iconCount As Integer) As List(Of Icon)
            Dim iconList As New List(Of Icon)()

            Select Case size
                Case SystemIconSize.Large
                    ExtractEx(fileName, iconList, Nothing, firstIconIndex, iconCount)
                    Exit Select

                Case SystemIconSize.Small
                    ExtractEx(fileName, Nothing, iconList, firstIconIndex, iconCount)
                    Exit Select
                Case Else

                    Throw New ArgumentOutOfRangeException("Taille")
            End Select

            Return iconList
        End Function

        Public Shared Sub Extract(fileName As String, largeIcons As List(Of Icon), smallIcons As List(Of Icon))
            Dim iconCount As Integer = GetIconsCountInFile(fileName)
            ExtractEx(fileName, largeIcons, smallIcons, 0, iconCount)
        End Sub

        Public Shared Function Extract(fileName As String, size As SystemIconSize) As List(Of Icon)
            Dim iconCount As Integer = GetIconsCountInFile(fileName)
            Return ExtractEx(fileName, size, 0, iconCount)
        End Function

        Public Shared Function ExtractOne(fileName As String, index As Integer, size As SystemIconSize) As Icon
            Try
                Dim iconList As List(Of Icon) = ExtractEx(fileName, size, index, 1)
                Return iconList(0)
            Catch e As UnableToExtractIconsException
                Throw New IconNotFoundException(fileName, index, e)
            End Try
        End Function

        Public Shared Sub ExtractOne(fileName As String, index As Integer, largeIcon As Icon, smallIcon As Icon)
            Dim smallIconList = New List(Of Icon)()
            Dim largeIconList = New List(Of Icon)()
            Try
                ExtractEx(fileName, largeIconList, smallIconList, index, 1)
                largeIcon = largeIconList(0)
                smallIcon = smallIconList(0)
            Catch e As UnableToExtractIconsException
                Throw New IconNotFoundException(fileName, index, e)
            End Try
        End Sub

#End Region

        Private Shared Function GetExtensionIconStringFromKeyUsingDefaultIcon(key As RegistryKey) As String
            Debug.Assert(key IsNot Nothing)

            Using defaultIconKey = key.OpenSubKey("DefaultIcon", False)
                If defaultIconKey Is Nothing Then
                    Return Nothing
                End If

                Dim value = defaultIconKey.GetValue(Nothing)
                If value Is Nothing Then
                    Return Nothing
                End If

                Return value.ToString()
            End Using
        End Function

        Private Shared Function GetExtensionIconStringFromKeyFromClsid(classesRootKey As RegistryKey, clsid As String) As String
            Debug.Assert(classesRootKey IsNot Nothing)
            Debug.Assert(clsid IsNot Nothing)

            Using clsidKey = classesRootKey.OpenSubKey("CLSID", False)
                If clsidKey Is Nothing Then
                    Return Nothing
                End If

                Using applicationClsidKey = clsidKey.OpenSubKey(clsid, False)
                    If applicationClsidKey Is Nothing Then
                        Return Nothing
                    End If

                    Return GetExtensionIconStringFromKeyUsingDefaultIcon(applicationClsidKey)
                End Using
            End Using
        End Function

        Private Shared Function GetExtensionIconStringFromKeyUsingClsid(key As RegistryKey) As String
            Debug.Assert(key IsNot Nothing)

            Dim applicationClsid As String
            Using clsidKey = key.OpenSubKey("CLSID", False)
                If clsidKey Is Nothing Then
                    Return Nothing
                End If

                Dim value = clsidKey.GetValue(Nothing)
                If value Is Nothing Then
                    Return Nothing
                End If

                applicationClsid = value.ToString()
            End Using

            Using classesRootKey = Registry.ClassesRoot
                Dim fromNormalClsid = GetExtensionIconStringFromKeyFromClsid(classesRootKey, applicationClsid)
                If fromNormalClsid IsNot Nothing Then
                    Return fromNormalClsid
                End If

                Using wow6432ClassesRootKey = classesRootKey.OpenSubKey("Wow6432Node")
                    If wow6432ClassesRootKey Is Nothing Then
                        Return Nothing
                    End If

                    Return GetExtensionIconStringFromKeyFromClsid(wow6432ClassesRootKey, applicationClsid)
                End Using
            End Using
        End Function

        Private Shared Function GetExtensionIconStringFromRegistry(extension As String) As String
            Debug.Assert(extension IsNot Nothing)
            Debug.Assert(extension.Length > 1)
            Debug.Assert(extension.StartsWith("."))

            Using classesRootKey = Registry.ClassesRoot
                Using extensionKey = classesRootKey.OpenSubKey(extension, False)
                    If extensionKey Is Nothing Then
                        Return Nothing
                    End If

                    Dim fileTypeObject = extensionKey.GetValue(Nothing)

                    If fileTypeObject Is Nothing Then
                        Return Nothing
                    End If

                    Using applicationKey = classesRootKey.OpenSubKey(fileTypeObject.ToString(), False)
                        Dim result = GetExtensionIconStringFromKeyUsingDefaultIcon(applicationKey)
                        If result Is Nothing Then
                            result = GetExtensionIconStringFromKeyUsingClsid(applicationKey)
                        End If
                        Return result
                    End Using
                End Using
            End Using
        End Function

        Public Shared Function IconFromExtensionUsingRegistry(extension As String, size As SystemIconSize) As Icon
            If extension Is Nothing Then
                Throw New ArgumentNullException("extension")
            End If
            If extension.Length = 0 OrElse extension = "." Then
                Throw New ArgumentException("Empty extension", "extension")
            End If

            If extension(0) <> "."c Then
                extension = "."c & extension
            End If

            Dim iconLocation = GetExtensionIconStringFromRegistry(extension)

            If iconLocation Is Nothing Then
                Return Nothing
            End If

            Return ExtractFromRegistryString(iconLocation, size)
        End Function

        Public Shared Function IconFromExtension(extension As String, size As SystemIconSize) As Icon
            If extension Is Nothing Then
                Throw New ArgumentNullException("extension")
            End If
            If extension.Length = 0 OrElse extension = "." Then
                Throw New ArgumentException("Extension vide", "extension")
            End If

            If extension(0) <> "."c Then
                extension = "."c & extension
            End If

            Dim fileInfo = New NativeStruct.SHFILEINFO()
            NativeMethods.SHGetFileInfo(extension, 0, fileInfo, Marshal.SizeOf(fileInfo), NativeEnum.SHGFI.SHGFI_ICON Or NativeEnum.SHGFI.SHGFI_USEFILEATTRIBUTES Or DirectCast(size, NativeEnum.SHGFI))

            Return Icon.FromHandle(fileInfo.hIcon)
        End Function

        Public Shared Function IconFromResource(resourceName As String) As Icon
            Dim assembly__1 = Assembly.GetCallingAssembly()
            Dim stream = assembly__1.GetManifestResourceStream(resourceName)

            Return New Icon(stream)
        End Function

        Public Shared Function ExtractFromRegistryString(regString As String, size As SystemIconSize) As Icon
            Dim str() As String = splitPathIndex(regString)
            Try
                Return ExtractOne(str(0), str(1), size)
            Catch generatedExceptionName As IconNotFoundException
                Return Nothing
            End Try
        End Function


        Public Shared Function splitPathIndex(ByVal PathIndex As String) As String()
            Dim strArray2 As String() = New String(2 - 1) {}
            If (InStr(PathIndex, ",", CompareMethod.Binary) > 0) Then
                strArray2 = PathIndex.Split(New Char() {","c})
            Else
                strArray2(0) = PathIndex
                strArray2(1) = "0"
            End If
            If (IIf((strArray2(0).StartsWith("""") AndAlso strArray2(0).EndsWith("""")), 1, 0) <> 0) Then
                strArray2(0) = strArray2(0).Split(New Char() {""""c})(1)
            End If
            If (InStr(strArray2(0), ":\", CompareMethod.Binary) = 0) Then
                strArray2(0) = Environment.SystemDirectory & "\" & strArray2(0)
            End If
            Return strArray2
        End Function

    End Class
End Namespace