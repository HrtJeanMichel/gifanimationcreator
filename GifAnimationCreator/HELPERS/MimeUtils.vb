﻿Imports System.IO
Imports System.Drawing.Imaging

Namespace Helpers

    Friend NotInheritable Class ImageArgs

        Public Property Extension() As String
        Public Property imgFormat() As ImageFormat

        Public Sub New(ext As String, imgF As ImageFormat)
            _Extension = ext
            _imgFormat = imgF
        End Sub
    End Class

    Friend Class MimeTypes
        Public Const pngMime As String = "image/png"
        Public Const bmpMime As String = "image/bmp"
        Public Const gifMime As String = "image/gif"
        Public Const jpegMime As String = "image/jpeg"
        Public Const pngExt As String = ".png"
        Public Const bmpExt As String = ".bmp"
        Public Const gifExt As String = ".gif"
        Public Const jpegExt As String = ".jpeg"
        Public Const jpgExt As String = ".jpg"
        Public Shared pngFormat As ImageFormat = ImageFormat.Png
        Public Shared bmpFormat As ImageFormat = ImageFormat.Bmp
        Public Shared gifFormat As ImageFormat = ImageFormat.Gif
        Public Shared jpegFormat As ImageFormat = ImageFormat.Jpeg
    End Class

    Public Class MimeType

        Private m_dict0 As Dictionary(Of String, ImageArgs)
        Private m_dict1 As Dictionary(Of String, ImageFormat)

        Public Sub New()
            AddToDictionary0()
            AddToDictionary1()
        End Sub

        Private Sub AddToDictionary0()
            m_dict0 = New Dictionary(Of String, ImageArgs)
            m_dict0.Add(MimeTypes.pngMime, New ImageArgs(MimeTypes.pngExt, MimeTypes.pngFormat))
            m_dict0.Add(MimeTypes.bmpMime, New ImageArgs(MimeTypes.bmpExt, MimeTypes.bmpFormat))
            m_dict0.Add(MimeTypes.gifMime, New ImageArgs(MimeTypes.gifExt, MimeTypes.gifFormat))
            m_dict0.Add(MimeTypes.jpegMime, New ImageArgs(MimeTypes.jpegExt, MimeTypes.jpegFormat))
        End Sub

        Private Sub AddToDictionary1()
            m_dict1 = New Dictionary(Of String, ImageFormat)
            m_dict1.Add(MimeTypes.pngExt, MimeTypes.pngFormat)
            m_dict1.Add(MimeTypes.bmpExt, MimeTypes.bmpFormat)
            m_dict1.Add(MimeTypes.gifExt, MimeTypes.gifFormat)
            m_dict1.Add(MimeTypes.jpegExt, MimeTypes.jpegFormat)
            m_dict1.Add(MimeTypes.jpgExt, MimeTypes.jpegFormat)
        End Sub

        Public Function ExtOrImgFormat(MimeType As String, GetExtension As Boolean) As Object
            Dim result As Object = Nothing
            Dim pair0 As KeyValuePair(Of String, ImageArgs)
            For Each pair0 In m_dict0
                If pair0.Key = MimeType Then
                    If GetExtension Then
                        result = pair0.Value.Extension
                    Else
                        result = pair0.Value.imgFormat
                    End If
                End If
            Next
            Return result
        End Function

        Public Function GetImageFormat(Ext As String) As ImageFormat
            Dim result As ImageFormat = Nothing
            Dim pair0 As KeyValuePair(Of String, ImageFormat)
            For Each pair0 In m_dict1
                If pair0.Key = Ext Then
                    result = pair0.Value
                End If
            Next
            Return result
        End Function

        Private Function GetGuidID(filepath As String) As Guid
            Dim imagedata As Byte() = File.ReadAllBytes(filepath)
            Dim id As Guid
            Using ms As New MemoryStream(imagedata)
                Using img As Image = Image.FromStream(ms)
                    id = img.RawFormat.Guid
                End Using
            End Using
            Return id
        End Function

        Public Function isCorrectMIMEType(filepath As String) As Boolean
            Dim b As Boolean = False
            Try
                Dim id As Guid = GetGuidID(filepath)
                Return If(id = ImageFormat.Png.Guid OrElse id = ImageFormat.Bmp.Guid OrElse id = ImageFormat.Gif.Guid OrElse id = ImageFormat.Exif.Guid OrElse id = ImageFormat.Jpeg.Guid OrElse id = ImageFormat.MemoryBmp.Guid, True, False)
            Catch
            End Try
            Return b
        End Function

        Public Function GetMIMEType(filepath As String) As String
            Dim ResultmimeType As String = "image/unknown"
            Try
                Dim id As Guid = GetGuidID(filepath)
                If id = ImageFormat.Png.Guid Then
                    ResultmimeType = MimeTypes.pngMime
                ElseIf id = ImageFormat.Bmp.Guid Then
                    ResultmimeType = MimeTypes.bmpMime
                ElseIf id = ImageFormat.Gif.Guid Then
                    ResultmimeType = MimeTypes.gifMime
                ElseIf id = ImageFormat.Exif.Guid Then
                    ResultmimeType = MimeTypes.jpegMime
                ElseIf id = ImageFormat.Jpeg.Guid Then
                    ResultmimeType = MimeTypes.jpegMime
                ElseIf id = ImageFormat.MemoryBmp.Guid Then
                    ResultmimeType = MimeTypes.bmpMime
                End If
            Catch
            End Try
            Return ResultmimeType
        End Function

    End Class


End Namespace