﻿Imports System.Drawing.Drawing2D
Imports System.Drawing.Imaging
Imports System.IO

Namespace Helpers
    Public Class Images

        Public Shared Function ScaleImage(image As Image, maxWidth As Integer, maxHeight As Integer) As Image
            Dim transparentImg = New Bitmap(maxWidth, maxHeight, PixelFormat.Format32bppArgb)
            Dim x = (maxWidth / 2) - (image.Width / 2)
            Dim y = (maxHeight / 2) - (image.Height / 2)
            Using g = Graphics.FromImage(transparentImg)
                'g.Clear(Color.Transparent)
                g.Clear(Color.White)
            End Using

            Using grD As Graphics = Graphics.FromImage(transparentImg)
                Dim rect = New Rectangle(0, 0, image.Width, image.Height)
                Dim destrect = New Rectangle(x, y, image.Width, image.Height)
                grD.DrawImage(image, destrect, rect, GraphicsUnit.Pixel)
            End Using
            Return transparentImg
        End Function

        Public Shared Function DrawBitmapWithBorder(bmp As Bitmap, ByVal Optional borderSize As Integer = 2) As Bitmap
            Dim newWidth As Integer = bmp.Width + (borderSize * 2)
            Dim newHeight As Integer = bmp.Height + (borderSize * 2)
            Dim newImage As Image = New Bitmap(newWidth, newHeight)

            Using gfx As Graphics = Graphics.FromImage(newImage)

                Using border As Brush = New SolidBrush(Color.Black)
                    gfx.FillRectangle(border, 0, 0, newWidth, newHeight)
                End Using

                gfx.DrawImage(bmp, New Rectangle(borderSize, borderSize, bmp.Width, bmp.Height))
            End Using

            Return CType(newImage, Bitmap)
        End Function

        Public Shared Sub AutosizeImg(ImagePath As String, Pbx As PictureBox, Optional ByVal pSizeMode As PictureBoxSizeMode = PictureBoxSizeMode.CenterImage)
            Try
                Pbx.Image = Nothing
                Pbx.SizeMode = pSizeMode
                If File.Exists(ImagePath) Then
                    Pbx.Image = GetAutoSizeImage(Image.FromFile(ImagePath), Pbx.Size)
                Else
                    Pbx.Image = Nothing
                End If
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Information)
            End Try
        End Sub

        Private Shared Function GetAutoSizeImage(ImagePath As Image, siz As Size) As Image
            Dim imgOrg As Bitmap
            Dim imgShow As Bitmap
            Dim g As Graphics
            Dim divideBy, divideByH, divideByW As Double
            imgOrg = ImagePath
            divideByW = imgOrg.Width / siz.Width
            divideByH = imgOrg.Height / siz.Height
            If divideByW > 1 Or divideByH > 1 Then
                If divideByW > divideByH Then
                    divideBy = divideByW
                Else
                    divideBy = divideByH
                End If
                imgShow = New Bitmap(CInt(CDbl(imgOrg.Width) / divideBy), CInt(CDbl(imgOrg.Height) / divideBy))
                imgShow.SetResolution(imgOrg.HorizontalResolution, imgOrg.VerticalResolution)
                g = Graphics.FromImage(imgShow)
                g.SmoothingMode = SmoothingMode.HighQuality
                g.InterpolationMode = InterpolationMode.HighQualityBicubic
                g.DrawImage(imgOrg, New Rectangle(0, 0, CInt(CDbl(imgOrg.Width) / divideBy), CInt(CDbl(imgOrg.Height) / divideBy)), 0, 0, imgOrg.Width, imgOrg.Height, GraphicsUnit.Pixel)
                g.Dispose()
            Else
                imgShow = New Bitmap(imgOrg.Width, imgOrg.Height)
                imgShow.SetResolution(imgOrg.HorizontalResolution, imgOrg.VerticalResolution)
                g = Graphics.FromImage(imgShow)
                g.SmoothingMode = SmoothingMode.HighQuality
                g.InterpolationMode = InterpolationMode.HighQualityBicubic
                g.DrawImage(imgOrg, New Rectangle(0, 0, imgOrg.Width, imgOrg.Height), 0, 0, imgOrg.Width, imgOrg.Height, GraphicsUnit.Pixel)
                g.Dispose()
            End If
            imgOrg.Dispose()
            Return imgShow
        End Function
    End Class
End Namespace
