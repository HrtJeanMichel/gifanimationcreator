# **Gif Animation Creator** #

# Description

Cet outil permet de générer un fichier animé au formation GIF à partir d'une sélection d'images (jpg, bmp, png, gif).

# Screenshot

![GifAnimationCreator.gif](https://i.imgur.com/dmfQrLv.gif)


# Features

* Ajout de fichiers : jpg, bmp, png, gif
* Prévisualisation des images de la liste
* Ordonnancement de la liste des fichiers par bouton ou glissé/déposé des éléments
* Paramétrage du délai d'animation
* Ajout ou non d'un contour sur chaque image avant la génération du GIF (pratique pour les images blanches sur fond blanc)


# Prerequisites

* Tout OS Windows
* DotNet Framework 4.5
* Pas d'installation


# WebSite

* [http://3dotdevcoder.blogspot.fr/](http://3dotdevcoder.blogspot.fr/)


# Credits

* Cyotek : Pour l'extension du contrôle ListView [Dragging ListviewItem] (http://www.cyotek.com/blog/dragging-items-in-a-listview-control-with-visual-insertion-guides)
* Julien Roncaglia : Pour sa classe de gestion des icones [IconEx](https://stackoverflow.com/users/story/46594)


# Copyright

Copyright © HrtJm 2008-2020


# Licence

